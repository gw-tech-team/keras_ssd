import keras
import pickle
from module.video import VideoTest
import os
import pathlib
c_d = pathlib.Path(__file__).resolve().parent
import sys
sys.path.append(str(c_d) + '/../01_train/')

def find_all_files(directory,extention="jpg"):
    filelist=[]
    for root, dirs, files in os.walk(directory):
        for file in files:
            #file[-3:] means "3 char from the end of line"
            if file[-3:] == extention:
                filelist.append(os.path.join(root, file))
    return filelist

model_name="MobileNetSSD512"

if model_name=="VGG16SSD512":
    from model.ssd512VGG16 import SSD
    weight_name='weights.03-1.06.hdf5'

if model_name=="VGG16SSD300":
    from model.ssd300VGG16 import SSD
    # weight_name='VGG16SSD300weights_voc_2007_class20.hdf5'
    # weight_name = "weights.02-1.08.hdf5"
    weight_name = "weights.18-1.06.hdf5"
    print("selected")

if model_name == "FeatureFusedSSD300":
    from model.ssd300FeatureFused import SSD
    weight_name='FeatureFusedSSD300weights_voc_2007_class20.hdf5'

if model_name=="MobileNetSSD300":
    from model.ssd300MobileNet import SSD
    # weight_name = 'MobileNetSSD300weights_voc_2007_class20.hdf5'
    weight_name = "weights.03-2.07.hdf5"

if model_name=="MobileNetSSD512":
    from model.ssd512MobileNet import SSD
    # weight_name = 'MobileNetSSD300weights_voc_2007_class20.hdf5'
    weight_name = "mobile_v1_7_weights.50-1.44.hdf5"

if model_name=="MobileNetSSD512v2":
    from model.ssd512MobileNetv2 import SSD
    # weight_name = 'MobileNetSSD300weights_voc_2007_class20.hdf5'
    weight_name = "weights.02-1.40.hdf5"

if model_name=="Xception":
    from model.ssd300XceptionLite import SSD
    weight_name='XceptionSSDLite300weights_voc_2007_class20.hdf5'

class_file = "../00_create_dataset/Original/csp_dataset_class_7/classes.txt"
output_dir="./result/mobilenet_v1/"
result_name = "test1"
extention = ".avi"
input_path = "./videos/86_survibal1_sude_2person.avi"
num_class = 7
weights_path = './weights/0705/mobile/'
input_shape = (512,512,3)

g = open(class_file)
class_data = g.read()
print(class_data)
class_data = class_data.split("\n")
print("class_data",class_data)
class_data.pop(num_class)
class_data.insert(0, "BG")
print("class_data_2",class_data)
NUM_CLASSES = len(class_data)
print("NUM_CLASSES:",NUM_CLASSES)

model = SSD(input_shape, num_classes=NUM_CLASSES)
model.load_weights(weights_path+weight_name)

# paths = find_all_files("../csp_dataset_class_5/train/",extention="jpg")
# vid_test.run_img(paths)

vid_test = VideoTest(class_data, model, input_shape)
vid_test.run_video(input_path,output_path=output_dir + result_name + extention,conf_thresh = 0.5)
