import os
import xml.etree.ElementTree as et
from xml.dom import minidom as md

class converter():
    def __init__(self,
                format,
                img_path,
                width,
                height,
                depth,
                class_list_path,
                output_path
                ):
        self.format = format
        self.img_name = os.path.basename(img_path)
        self.img_path = img_path
        self.width = width
        self.height = height
        self.depth = depth
        self.class_list_path = class_list_path
        self.xml_name = self.img_name.split(".")[0] + ".xml"
        self.output_path = output_path
        self.encode = "utf-8"

    def get_class_data(self):
        class_file = self.class_list_path
        g = open(class_file)
        class_data = g.read()
        class_data = class_data.split("\n")
        return class_data

    # def _set_object(self,root,bbox):
    #     class_data = self.get_class_data()
    #     id = bbox[4]
    #     root.findall("object")[0][0].text = class_data[id]
    #     bbox_xml = root.findall("object")[0][4]
    #     bbox_xml[0].text = str(bbox[0])
    #     bbox_xml[1].text = str(bbox[1])
    #     bbox_xml[2].text = str(bbox[2])
    #     bbox_xml[3].text = str(bbox[3])
    #     return root

    def _create_object(self,root,bbox):
        class_data = self.get_class_data()
        object = et.SubElement(root, 'object')
        name = et.SubElement(object, 'name')
        if type(bbox) == str:
            bbox = bbox.split(",")
        else:
            bbox = bbox[0]
            bbox = bbox.split(",")
        class_id = bbox[4]
        name.text = class_data[int(class_id)]
        pose = et.SubElement(object, 'pose')
        pose.text = 'Unspecified'
        truncated = et.SubElement(object, 'truncated')
        truncated.text = '0'
        difficult = et.SubElement(object, 'difficult')
        difficult.text = '0'
        bndbox = et.SubElement(object, 'bndbox')
        # xmin = et.SubElement(bndbox, 'xmin')
        # xmin.text = str(int(bbox[0])*300/512)
        # ymin = et.SubElement(bndbox, 'ymin')
        # ymin.text = str(int(bbox[1])*300/512)
        # xmax = et.SubElement(bndbox, 'xmax')
        # xmax.text = str(int(bbox[2])*300/512)
        # ymax = et.SubElement(bndbox, 'ymax')
        # ymax.text = str(int(bbox[3])*300/512)
        xmin = et.SubElement(bndbox, 'xmin')
        xmin.text = str(bbox[0])
        ymin = et.SubElement(bndbox, 'ymin')
        ymin.text = str(bbox[1])
        xmax = et.SubElement(bndbox, 'xmax')
        xmax.text = str(bbox[2])
        ymax = et.SubElement(bndbox, 'ymax')
        ymax.text = str(bbox[3])
        return root

    def _fixed_data(self,root):
        root.findall("folder")[0].text = " "
        root.findall("filename")[0].text = self.img_name
        root.findall("path")[0].text = self.img_path
        size = root.findall("size")
        size[0][0].text = self.width
        size[0][1].text=self.height
        size[0][2].text = self.depth
        return root

    def txt2xml(self,bbox_list):
        tree = et.parse(self.format)
        root = tree.getroot()
        root = self._fixed_data(root)
        if len(bbox_list) == 1:
            root = self._create_object(root,bbox_list)
        elif 1 < len(bbox_list):
            for i, box in enumerate(bbox_list):
                    root = self._create_object(root,bbox_list[i])
        if not os.path.exists(self.output_path):
            os.mkdir(self.output_path)
        document = md.parseString(et.tostring(root, self.encode))
        with open(self.output_path + "/" + self.xml_name, mode='w', encoding=self.encode) as h:
            document.writexml(
                                h,
                                encoding = self.encode,
                                newl = "\n",
                                indent = "",
                                addindent = "\t"
                                )

if __name__ =="__main__":
    traintxt = "./csp_dataset_class_7/train/train.txt"
    box = [10,9,8,10,2]
    bbox = [[10,9,8,10,2],[5,3,4,1,2]]
    convert = converter(format='./format.xml',
                img_path="test_hi2.jpg",
                width="512",
                height="512",
                depth="3",
                class_list_path="./csp_dataset_class_7/classes.txt")
    convert.txt2xml(bbox)
