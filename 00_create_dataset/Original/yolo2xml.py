from modules.create_xml import converter
from tqdm import tqdm

XML_FILE_PATH = "../XML_FILES/"

if __name__ == "__main__":
    class_file = "./csp_dataset_class_7/classes.txt"
    train_txt_file = "./csp_dataset_class_7/val/train.txt"
    output_path = "test"
    img_shape = (512,512,3)
    g = open(class_file)
    class_data = g.read()
    print("class_data:",class_data)
    class_data = class_data.split("\n")
    print("len(class_data)",len(class_data)-1)
    f = open(train_txt_file)
    txt_data = f.read()
    txt_data = txt_data.split("\n")
    print("len(train_txt_data)",len(txt_data)-1)
    for i in tqdm(range(len(txt_data))):
        path_n_bb = txt_data[i].split(" ")
        if path_n_bb[0] != "":
            path = path_n_bb[0]
            convert = converter(format='./modules/format.xml',
                        img_path=path,
                        width=str(img_shape[0]),
                        height=str(img_shape[1]),
                        depth=str(img_shape[2]),
                        class_list_path=class_file,
                        output_path= XML_FILE_PATH + output_path
                        )
            path_n_bb.pop(0)
            convert.txt2xml(path_n_bb)
