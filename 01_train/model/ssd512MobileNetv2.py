"""Keras implementation of SSD."""

import keras.backend as K
from keras.layers import Activation
from keras.layers import Conv2D,SeparableConv2D
from keras.layers import Dropout,BatchNormalization
from keras.layers import AlphaDropout,GaussianDropout
from keras.layers import Flatten
from keras.layers import Input
from keras.layers import Activation
from keras.layers.merge import concatenate
from keras.layers import Reshape
from keras.models import Model
import pathlib
c_d = pathlib.Path(__file__).resolve().parent
import sys
sys.path.append(str(c_d) + '/../modules/')
from ssd_layers import PriorBox
from keras.models import Sequential
from keras.applications import MobileNet




def SSD(input_shape, num_classes):
    """SSD300 MobileNet architecture.

    # Arguments
        input_shape: Shape of the input image,
            expected to be either (300, 300, 3) or (3, 300, 300)(not tested).
        num_classes: Number of classes including background.

    # References
        https://github.com/chuanqi305/MobileNet-SSD
    """
    img_size=(input_shape[1],input_shape[0])
    input_shape=(input_shape[1],input_shape[0],3)
    mobilenet_input_shape=(224,224,3)

    net = {}
    net['input'] = Input(input_shape)
    mobilenet=MobileNet(input_shape=mobilenet_input_shape,include_top=False,weights='imagenet')
    FeatureExtractor=Model(inputs=mobilenet.input, outputs=mobilenet.get_layer('conv_pw_5_relu').output)

    #(64,64)
    net['mobilenet_conv_dw_11_relu']= FeatureExtractor(net['input'])
    net['conv11'] = Conv2D(256, (1, 1),  padding='same', name='conv11')(net['mobilenet_conv_dw_11_relu'])
    net['conv11'] = BatchNormalization( momentum=0.99, name='bn11')(net['conv11'])
    net['conv11'] = Activation('relu')(net['conv11'])

    #(64,64) -> (32,32)
    net['conv12dw'] = SeparableConv2D(512, (3, 3),strides=(2, 2),  padding='same', name='conv12dw')(net['conv11'])
    net['conv12dw'] = BatchNormalization( momentum=0.99, name='bn12dw')(net['conv12dw'])
    net['conv12dw'] = Activation('relu')(net['conv12dw'])

    #(32,32)
    net['conv12'] = Conv2D(512, (1, 1), padding='same',name='conv12')(net['conv12dw'])
    net['conv12'] = BatchNormalization( momentum=0.99, name='bn12')(net['conv12'])
    net['conv12'] = Activation('relu')(net['conv12'])

    #(32,32)
    net['conv13dw'] = SeparableConv2D(512, (3, 3), padding='same',name='conv13dw')(net['conv12'])
    net['conv13dw'] = BatchNormalization( momentum=0.99, name='bn13dw')(net['conv13dw'])
    net['conv13dw'] = Activation('relu')(net['conv13dw'])

    #(32,32)
    net['conv14'] = Conv2D(512, (1, 1), padding='same',name='conv14')(net['conv13dw'])
    net['conv14'] = BatchNormalization( momentum=0.99, name='bn14')(net['conv14'])
    net['conv14'] = Activation('relu')(net['conv14'])

    #(32,32)
    net['conv14dw'] = SeparableConv2D(512, (3, 3), padding='same',name='conv14dw')(net['conv14'])
    net['conv14dw'] = BatchNormalization( momentum=0.99, name='bn14dw')(net['conv14dw'])
    net['conv14dw'] = Activation('relu')(net['conv14dw'])

    #(32,32)
    net['conv15'] = Conv2D(512, (1, 1), padding='same',name='conv15')(net['conv14dw'])
    net['conv15'] = BatchNormalization( momentum=0.99, name='bn15')(net['conv15'])
    net['conv15'] = Activation('relu')(net['conv15'])

    #(32,32)
    net['conv16_1'] = Conv2D(256, (1, 1),  padding='same', name='conv16_1')(net['conv15'])
    net['conv16_1'] = BatchNormalization( momentum=0.99, name='bn16_1')(net['conv16_1'])
    net['conv16_1'] = Activation('relu')(net['conv16_1'])

    #(32,32) -> (16,16)
    net['conv17dw'] = SeparableConv2D(512, (3, 3),strides=(2, 2),  padding='same', name='conv17dw')(net['conv16_1'])
    net['conv17dw'] = BatchNormalization( momentum=0.99, name='bn17dw')(net['conv17dw'])
    net['conv17dw'] = Activation('relu')(net['conv17dw'])

    #(16,16)
    net['conv18'] = Conv2D(1024, (1, 1), padding='same',name='conv18')(net['conv17dw'])
    net['conv18'] = BatchNormalization( momentum=0.99, name='bn18')(net['conv18'])
    net['conv18'] = Activation('relu')(net['conv18'])

    #(16,16)
    net['conv19dw'] = SeparableConv2D(1024, (3, 3), padding='same',name='conv19dw')(net['conv18'])
    net['conv19dw'] = BatchNormalization( momentum=0.99, name='bn19dw')(net['conv19dw'])
    net['conv19dw'] = Activation('relu')(net['conv19dw'])

    #(16,16)
    net['conv20'] = Conv2D(1024, (1, 1), padding='same',name='conv20')(net['conv19dw'])
    net['conv20'] = BatchNormalization( momentum=0.99, name='bn20')(net['conv20'])
    net['conv20'] = Activation('relu')(net['conv20'])

    #(16,16)
    net['conv21_1'] = Conv2D(256, (1, 1),  padding='same', name='conv21_1')(net['conv20'])
    net['conv21_1'] = BatchNormalization( momentum=0.99, name='bn21_1')(net['conv21_1'])
    net['conv21_1'] = Activation('relu')(net['conv21_1'])

    #(16,16) -> (8,8)
    net['conv21_2'] = Conv2D(512, (3, 3), strides=(2, 2),  padding='same', name='conv21_2')(net['conv21_1'])
    net['conv21_2'] = BatchNormalization( momentum=0.99, name='bn21_2')(net['conv21_2'])
    net['conv21_2'] = Activation('relu')(net['conv21_2'])

    #(8,8)
    net['conv22_1'] = Conv2D(128, (1, 1), padding='same',name='conv22_1')(net['conv21_2'])
    net['conv22_1'] = BatchNormalization( momentum=0.99, name='bn22_1')(net['conv22_1'])
    net['conv22_1'] = Activation('relu')(net['conv22_1'])

    #(8,8) -> (4,4)
    net['conv22_2'] = Conv2D(256, (3, 3), strides=(2, 2), padding='same',name='conv22_2')(net['conv22_1'])
    net['conv22_2'] = BatchNormalization( momentum=0.99, name='bn15_2')(net['conv22_2'])
    net['conv22_2'] = Activation('relu')(net['conv22_2'])

    #(4,4)
    net['conv23_1'] = Conv2D(128, (1, 1),  padding='same', name='conv23_1')(net['conv22_2'])
    net['conv23_1'] = BatchNormalization( momentum=0.99, name='bn23_1')(net['conv23_1'])
    net['conv23_1'] = Activation('relu')(net['conv23_1'])

    #(2,2)
    net['conv23_2'] = Conv2D(256, (3, 3), strides=(2, 2),  padding='same', name='conv23_2')(net['conv23_1'])
    net['conv23_2'] = BatchNormalization( momentum=0.99, name='bn23_2')(net['conv23_2'])
    net['conv23_2'] = Activation('relu')(net['conv23_2'])

    # (2,2)
    net['conv24_1'] = Conv2D(64, (1, 1),  padding='same', name='conv24_1')(net['conv23_2'])
    net['conv24_1'] = BatchNormalization( momentum=0.99, name='bn24_1')(net['conv24_1'])
    net['conv24_1'] = Activation('relu')(net['conv24_1'])

    # (1,1)
    net['conv24_2'] = Conv2D(128, (3, 3), strides=(2, 2),  padding='same', name='conv24_2')(net['conv24_1'])
    net['conv24_2'] = BatchNormalization( momentum=0.99, name='bn24_2')(net['conv24_2'])
    net['conv24_2'] = Activation('relu')(net['conv24_2'])


    #Prediction from conv11 (64,64)
    num_priors = 4
    x = Conv2D(num_priors * 4, (1,1), padding='same',name='conv11_mbox_loc')(net['conv11'])
    net['conv11_mbox_loc'] = x
    flatten = Flatten(name='conv11_mbox_loc_flat')
    net['conv11_mbox_loc_flat'] = flatten(net['conv11_mbox_loc'])
    name = 'conv11_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Conv2D(num_priors * num_classes, (1,1), padding='same',name=name)(net['conv11'])
    net['conv11_mbox_conf'] = x
    flatten = Flatten(name='conv11_mbox_conf_flat')
    net['conv11_mbox_conf_flat'] = flatten(net['conv11_mbox_conf'])
    priorbox = PriorBox(img_size,35.84,max_size=76.8, aspect_ratios=[2],variances=[0.1, 0.1, 0.2, 0.2],name='conv11_mbox_priorbox')
    net['conv11_mbox_priorbox'] = priorbox(net['conv11'])



    # Prediction from conv15 (32,32)
    num_priors = 6
    x = Conv2D(num_priors * 4, (1,1),padding='same',name='conv15_mbox_loc')(net['conv15'])
    net['conv15_mbox_loc'] = x
    flatten = Flatten(name='conv15_mbox_loc_flat')
    net['conv15_mbox_loc_flat'] = flatten(net['conv15_mbox_loc'])
    name = 'conv15_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    net['conv15_mbox_conf'] = Conv2D(num_priors * num_classes, (1,1),padding='same',name=name)(net['conv15'])
    flatten = Flatten(name='conv15_mbox_conf_flat')
    net['conv15_mbox_conf_flat'] = flatten(net['conv15_mbox_conf'])
    priorbox = PriorBox(img_size, 76.8, max_size=153.6, aspect_ratios=[2, 3],variances=[0.1, 0.1, 0.2, 0.2],name='conv15_mbox_priorbox')
    net['conv15_mbox_priorbox'] = priorbox(net['conv15'])



    # Prediction from conv17dw (16,16)
    num_priors = 6
    x = Conv2D(num_priors * 4, (1,1), padding='same',name='conv17dw_2_mbox_loc')(net['conv17dw'])
    net['conv17dw_2_mbox_loc'] = x
    flatten = Flatten(name='conv17dw_2_mbox_loc_flat')
    net['conv17dw_2_mbox_loc_flat'] = flatten(net['conv17dw_2_mbox_loc'])
    name = 'conv17dw_2_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Conv2D(num_priors * num_classes, (1,1), padding='same',name=name)(net['conv17dw'])
    net['conv17dw_2_mbox_conf'] = x
    flatten = Flatten(name='conv17dw_2_mbox_conf_flat')
    net['conv17dw_2_mbox_conf_flat'] = flatten(net['conv17dw_2_mbox_conf'])
    priorbox = PriorBox(img_size, 153.6, max_size=230.4, aspect_ratios=[2, 3],variances=[0.1, 0.1, 0.2, 0.2],name='conv17dw_2_mbox_priorbox')
    net['conv17dw_2_mbox_priorbox'] = priorbox(net['conv17dw'])



    # Prediction from conv21_2 (8,8)
    num_priors = 6
    x = Conv2D(num_priors * 4, (1,1), padding='same',name='conv21_2_mbox_loc')(net['conv21_2'])
    net['conv21_2_mbox_loc'] = x
    flatten = Flatten(name='conv21_2_mbox_loc_flat')
    net['conv21_2_mbox_loc_flat'] = flatten(net['conv21_2_mbox_loc'])
    name = 'conv21_2_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Conv2D(num_priors * num_classes, (1,1), padding='same',name=name)(net['conv21_2'])
    net['conv21_2_mbox_conf'] = x
    flatten = Flatten(name='conv21_2_mbox_conf_flat')
    net['conv21_2_mbox_conf_flat'] = flatten(net['conv21_2_mbox_conf'])
    priorbox = PriorBox(img_size, 230.4, max_size=307.2, aspect_ratios=[2, 3],variances=[0.1, 0.1, 0.2, 0.2],name='conv21_2_mbox_priorbox')
    net['conv21_2_mbox_priorbox'] = priorbox(net['conv21_2'])


    # Prediction from conv22_2 (4,4)
    num_priors = 6
    x = Conv2D(num_priors * 4, (1,1), padding='same',name='conv22_2_mbox_loc')(net['conv22_2'])
    net['conv22_2_mbox_loc'] = x
    flatten = Flatten(name='conv22_2_mbox_loc_flat')
    net['conv22_2_mbox_loc_flat'] = flatten(net['conv22_2_mbox_loc'])
    name = 'conv22_2_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Conv2D(num_priors * num_classes, (1,1), padding='same',name=name)(net['conv22_2'])
    net['conv22_2_mbox_conf'] = x
    flatten = Flatten(name='conv22_2_mbox_conf_flat')
    net['conv22_2_mbox_conf_flat'] = flatten(net['conv22_2_mbox_conf'])
    priorbox = PriorBox(img_size, 307.2, max_size=384.0, aspect_ratios=[2, 3],variances=[0.1, 0.1, 0.2, 0.2],name='conv22_2_mbox_priorbox')
    net['conv22_2_mbox_priorbox'] = priorbox(net['conv22_2'])



    # Prediction from conv23_2 (2,2)
    num_priors = 6
    x = Conv2D(num_priors * 4,(1, 1), padding='same', name='conv23_2_mbox_loc')(net['conv23_2'])
    net['conv23_2_mbox_loc'] = x
    flatten = Flatten(name='conv17_2_mbox_loc_flat')
    net['conv23_2_mbox_loc_flat'] = flatten(net['conv23_2_mbox_loc'])
    name = 'conv23_2_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Conv2D(num_priors * num_classes, (1,1), padding='same', name=name)(net['conv23_2'])
    net['conv23_2_mbox_conf'] = x
    flatten = Flatten(name='conv23_2_mbox_conf_flat')
    net['conv23_2_mbox_conf_flat'] = flatten(net['conv23_2_mbox_conf'])
    priorbox = PriorBox(img_size, 384.0, max_size=460.8, aspect_ratios=[2, 3], variances=[0.1, 0.1, 0.2, 0.2],name='conv23_2_mbox_priorbox')
    net['conv23_2_mbox_priorbox'] = priorbox(net['conv23_2'])


    # Prediction from conv24_2
    num_priors = 6
    x = Conv2D(num_priors * 4,(1, 1), padding='same', name='conv24_2_mbox_loc')(net['conv24_2'])
    net['conv24_2_mbox_loc'] = x
    flatten = Flatten(name='conv24_2_mbox_loc_flat')
    net['conv24_2_mbox_loc_flat'] = flatten(net['conv24_2_mbox_loc'])
    name = 'conv24_2_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Conv2D(num_priors * num_classes, (1,1), padding='same', name=name)(net['conv24_2'])
    net['conv24_2_mbox_conf'] = x
    flatten = Flatten(name='conv24_2_mbox_conf_flat')
    net['conv24_2_mbox_conf_flat'] = flatten(net['conv24_2_mbox_conf'])
    priorbox = PriorBox(img_size, 460.8, max_size=537.6, aspect_ratios=[2, 3], variances=[0.1, 0.1, 0.2, 0.2],name='conv24_2_mbox_priorbox')
    net['conv24_2_mbox_priorbox'] = priorbox(net['conv24_2'])


    # Gather all predictions
    net['mbox_loc'] = concatenate([net['conv11_mbox_loc_flat'],net['conv15_mbox_loc_flat'],net['conv17dw_2_mbox_loc_flat'],net['conv21_2_mbox_loc_flat'],net['conv22_2_mbox_loc_flat'],net['conv23_2_mbox_loc_flat'],net['conv24_2_mbox_loc_flat']],axis=1, name='mbox_loc')
    net['mbox_conf'] = concatenate([net['conv11_mbox_conf_flat'],net['conv15_mbox_conf_flat'],net['conv17dw_2_mbox_conf_flat'],net['conv21_2_mbox_conf_flat'],net['conv22_2_mbox_conf_flat'],net['conv23_2_mbox_conf_flat'],net['conv24_2_mbox_conf_flat']],axis=1, name='mbox_conf')
    net['mbox_priorbox'] = concatenate([net['conv11_mbox_priorbox'],net['conv15_mbox_priorbox'],net['conv17dw_2_mbox_priorbox'],net['conv21_2_mbox_priorbox'],net['conv22_2_mbox_priorbox'],net['conv23_2_mbox_priorbox'],net['conv24_2_mbox_priorbox']],axis=1,name='mbox_priorbox')
    if hasattr(net['mbox_loc'], '_keras_shape'):
        num_boxes = net['mbox_loc']._keras_shape[-1] // 4
    elif hasattr(net['mbox_loc'], 'int_shape'):
        num_boxes = K.int_shape(net['mbox_loc'])[-1] // 4
    net['mbox_loc'] = Reshape((num_boxes, 4),name='mbox_loc_final')(net['mbox_loc'])
    net['mbox_conf'] = Reshape((num_boxes, num_classes),name='mbox_conf_logits')(net['mbox_conf'])
    net['mbox_conf'] = Activation('softmax',name='mbox_conf_final')(net['mbox_conf'])
    net['predictions'] = concatenate([net['mbox_loc'],net['mbox_conf'],net['mbox_priorbox']],axis=2,name='predictions')
    model = Model(inputs=net['input'], outputs=net['predictions'])
    return model
