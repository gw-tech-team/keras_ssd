# Keras Implemention of CustomNetwork-SSD

## 使い方



### 目次
#### 1. データセット作成　(00_create_dataset)
#### 2. トレーニング (01_train)
#### 3. 推論 (2_prediction)


--------------------------


### 1. **データセット作成 (00_create_dataset)**
#### 1-1. Original/Yolo2XML.py
    yolo用のtrain.txtからxmlファイルを`XML_FILES`配下に生成 (trainとval用に２つ作成)



#### 1-2. PKL_FILES/create_pickle.py
    SSD用に、xmlファイルを[xmin,ymin,xmax,ymax,class0,class1,...]バウンディングボックスと
    クラスのone hot vector型に変換し、pickleして保存。


####  1-3. priorFiles/create_prior_box.py
    priorbox(SSD用アンカー)を、使用するネットワークのバウンディングボックス数と検知対象のサイズに合わせて生成
    JFYI (https://medium.com/@smallfishbigsea/understand-ssd-and-implement-your-own-caa3232cd6ad)
```
    e.g.1　SSD300MobileNet (keras 2.2.4)
    参考: https://qiita.com/tanakataiki/items/41509e1b0f4a9dcd01b1
    1 * 1 * 6 = 6
    2 * 2 * 6 = 24
    3 * 3 * 6 = 54
    5 * 5 * 6 = 150
    9 * 9 * 6 = 486
    18 * 18 * 3 = 972
    972 + 486 + 150 + 54 + 24 + 6 = 1,692

    keras 2.1.5の場合
    1 * 1 * 6 = 6
    2 * 2 * 6 = 24
    3 * 3 * 6 = 54
    5 * 5 * 6 = 150
    10 * 10 * 6 = 600
    19 * 19 * 3 = 1,083
    1,083 + 600 + 150 + 54 + 24 + 6 = 1,917

    e.g.2　SSD512MobileNet (keras 2.2.4)
    参考: priorFiles/layers/layer_mobilenet_512.md
    1 * 1 * 6 = 6
    2 * 2 * 6 = 24
    4 * 4 * 6 = 96
    8 * 8 * 6 = 384
    16 * 16 * 6 = 1536
    32 * 32 * 6 = 6144
    64 * 64 * 4 = 16384
    16384 + 6144 + 1536 + 384 + 96 + 24 + 6 = 24574
```  


----------------------------------


### 2. **トレーニング (01_train)**
####  2-1. train.py
    使用したいfeature extractor用ネットワークによってmodeを選択。


---------------------------------------


### 3. **推論 (02_prediction)**
####  3-1. prediction.py
    使用モデル、動画(画像)、ネットワーク、保存パスを指定し実行。
    一部01_trainにあるモジュールを使用しています。



--------------------------------


# Reference
### Original code
`https://github.com/tanakataiki/ssd_kerasV2`

### Pretrained model
`https://drive.google.com/drive/folders/1F8GjD3BFhf_hv9Ipez0twRptYc3P8YwP`

## Licence
The MIT License (MIT)

Copyright (c) 2018 Taiki Tanaka
